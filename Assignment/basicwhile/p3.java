//3: Write a program to count the digits of the given number.

class WhileDemo{

	public static void main(String A[]){

		int n=942111423;
		int count=0;
		while(n!=0){

			count++;
			n=n/10;

		}

		System.out.println("count of digit is:"+count);

	}
}
