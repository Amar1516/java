

/*Program 9: Write a program to reverse the given number.  */

class WhileDemo{


	public static void main(String []args){

		int n=942111423;
		int temp=n;

		int rev=0;
		int rem=0;

		while(temp!=0){

			rem=temp%10;
			rev=rev*10+rem;

			temp=temp/10;

		}
		System.out.println(n+"after reverse :="+rev);

	}
}
