/*  6: Write a program to print the sum of all even numbers and
multiplication of odd numbers between 1 to 10.   */

class WhileDemo{

	public static void main(String []args){

		int n=1;
		int sum=0;
		int prod=1;
		while(n!=11){

			if(n%2==0){

				sum=sum+n;
			}else{

				prod=prod*n;
			}

			n++;
		
		}
		System.out.println("sum of even no between 1 to 10 :"+sum);
		System.out.println("multiplication of odd no between 1 to 10:"+prod);
	}
}
