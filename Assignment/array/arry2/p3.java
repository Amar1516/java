import java.io.*;
class ArryDemo3{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the no of arry:");
		int size =Integer.parseInt(br.readLine());

		int arr []=new int[size];
		System.out.println("enter the elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		int eSum=0;
		int oSum=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]%2==0){

				eSum=eSum+arr[i];
			}else{

				oSum=oSum+arr[i];
			}	

		}
		System.out.println("sum of even no:"+eSum);
		System.out.println("sum of odd no:"+oSum);

	}
}
