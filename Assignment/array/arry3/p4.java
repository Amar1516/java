/*WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5
*/
import java.io.*;
class ArrayDemo3{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the arry size:");
		int size=Integer.parseInt(br.readLine());
		int ar[]=new int[size];
		System.out.println("enter the elements:");
		for(int i=0;i<ar.length;i++){

			ar[i]=Integer.parseInt(br.readLine());

			int count=0;
			for(int j=1;j<=ar[i];j++){

			if(ar[i]%j==0){

				count++;
			}
			}
			if(count==2){

				System.out.println("prime no"+""+ar[i]+"found at index"+""+i);

			}
		}

	}
}
