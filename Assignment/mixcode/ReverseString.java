/*6.Write a Program to reverse a given String.
Input String:
Ajay Bhosle
Output String:
elsohB yajA
*/

import java.io.*;

class Demo{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter the string:");

		String str=br.readLine();

		StringBuffer str1=new StringBuffer(str);

		System.out.println(str1.reverse());
	}
}


