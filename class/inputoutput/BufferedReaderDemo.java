
import java.io.*;
class BufferedReaderDemo{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter The First String :");
		String str1=br.readLine();
		System.out.println("String1 :"+str1);

		System.out.println("Enter The Second String :");
		String str2=br.readLine();
		System.out.println("String2:"+str2);

		br.close();             

		BufferedReader br2=new BufferedReader(new InputStreamReader(System.in));


		char ch=(char) br.read();  //Exception in thread "main" java.io.IOException: Stream closed
		
	//	char ch2=(char) br2.read();
		System.out.println("Char:"+ch);
		//System.out.println("Char:"+ch2);
		
	}
}
