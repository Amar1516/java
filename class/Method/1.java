class MethodDemo{
// Methods

	int x=19;
	static int y=29;
	
	
	static void fun(){

		System.out.println("in fun method");
	}

	void gun(){

		System.out.println("in gun method");

	}
	public static void main(String []args){

		fun();
	//	gun();    // non static method gun cannot be refrenced from static context
	
	
		MethodDemo obj= new MethodDemo();
		obj.gun();


	//	System.out.println(x);   //error: non-static variable x cannot be referenced from a static context
		System.out.println(y);
	

		System.out.println(obj.x);
	}

}
