

class MethodDemo{


        void  fun(int x){

		int y=x+10;
                return y;//error: incompatible types: unexpected return value
        }
        public static void main(String[]args){

		MethodDemo obj=new MethodDemo();
	//	System.out.println(obj.fun(10));      //  voin type not allowed here

		int a= obj.fun(19);    //error: incompatible types: void cannot be converted to int
		System.out.println(a);  
	}
}
