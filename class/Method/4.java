

class MethodDemo{


	void fun(int x){


		System.out.println(x);
	}

	public static void main(String[]args){

		System.out.println("in main");
		MethodDemo obj=new MethodDemo();
//  1		obj.fun();


		obj.fun(10);
		obj.fun(10.2f);   // error: incompatible types: possible lossy conversion from float to int
                obj.fun(true);   // error: boolean cannot be converted into  int
		System.out.println("end of main");

	}
}


/*  1  error: method fun in class MethodDemo cannot be applied to given types;
                obj.fun();
                   ^
  required: int
  found:    no arguments
  reason: actual and formal argument lists differ in length   */
