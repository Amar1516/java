

class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){

		super(tg,str);
	}
	public void run(){

		System.out.println(Thread.currentThread());   //  Thread[ThreadName,Priority,ThreadGroupName]
	}
}
class ThreadGroupDemo{

	public static void main(String []args){

		ThreadGroup parentTG = new ThreadGroup("Core2web");

		MyThread obj1 =new MyThread(parentTG,"C.Ds");
		MyThread obj2 =new MyThread(parentTG,"Java");
		MyThread obj3=new MyThread(parentTG,"Python");
		
		obj1.start();
		obj2.start();
		obj3.start();

		// create child Threadgroup

		ThreadGroup childTG=new ThreadGroup(parentTG,"Incubator");

		MyThread obj4=new MyThread(childTG,"Flutter");
		MyThread obj5=new MyThread(childTG,"Android");
		MyThread obj6=new MyThread(childTG,"FullStack");

		obj4.start();
		obj5.start();
		obj6.start();

	}
}
