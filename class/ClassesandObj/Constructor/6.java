

class Player{

	private int jerNo=0;
	private String name=null;

	Player(int jerNo,String name){		// internally:  Player(obj1,2,3*(this) ,jerNo,name)

		this.jerNo=jerNo;
		this.name=name;
		System.out.println("in constructor");
	}

	void info(){

		System.out.println(jerNo+"="+name);

	}
}

class Client{

	public static void main(String []args){

		Player obj1=new Player(18,"virat");	// internally:  Player(obj1,18,virat)
		obj1.info();				//internally:   info(obj1)

		Player obj2=new Player(7,"msd");	//internally:  Player(obj2,7,msd)
		obj2.info();				//internally:   info(obj2)
		
		Player obj3=new Player(45,"rohit");	//internally: Player(obj3,45,rohit)
		obj3.info();				//internally:  info(obj3)


	}
}
