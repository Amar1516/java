

class Player{

	private int jerNo=0;
	private String name=null;

	Player(int jerNo,String name){		//internally: Player(Player this)

		System.out.println(System.identityHashCode(this.name));
		this.jerNo=jerNo;
		this.name=name;
		System.out.println(System.identityHashCode(this.name));			// scp vr string aste
		System.out.println(System.identityHashCode(name));

		System.out.println("in constructor");
	}

	void info(){

		System.out.println(jerNo+"="+name);
	}


}

class Client{

	public static void main(String []args){

		Player obj1=new Player(18,"virat");
		obj1.info();

		Player obj2=new Player(7,"msd");
		obj2.info();
	}
}
