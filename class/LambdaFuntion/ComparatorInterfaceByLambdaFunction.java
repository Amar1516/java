
import java.util.*;

class Employee{

	int empId;
	String name;

	Employee(String name,int empId){

		this.name=name;
		this.empId=empId;
	}
	public String toString(){

		return empId+":"+name;
	}
}

class Demo{

	public static void main(String []args){
	
		ArrayList al=new ArrayList();

		al.add(new Employee("Kanha",15));
		al.add(new Employee("Ashish",25));
		al.add(new Employee("Rahul",84));

		System.out.println(al);

		Collections.sort(al,(obj1,obj2)->{

			return ((Employee)obj1).name.compareTo(((Employee)obj2).name);	
		});
		System.out.println(al);

	}
}

