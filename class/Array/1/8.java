class ArrayDemo{

	static void fun(int x , int y){

		System.out.println(x);//10
		System.out.println(System.identityHashCode(x));//

		System.out.println(y);//20
		System.out.println(System.identityHashCode(y));//


		x=x+10;
		y=y+10;

		System.out.println(x);//20
		System.out.println(System.identityHashCode(x));//
		System.out.println(y);//30
		System.out.println(System.identityHashCode(y));//
	}
	public static void main(String [] args){

		int x=10;
		int y=20;

		
		System.out.println(x);//10
		System.out.println(System.identityHashCode(x));//
		System.out.println(y);//20
		
		System.out.println(System.identityHashCode(y));//
	        
		fun(10,20);
		
		System.out.println(x);//10
		System.out.println(System.identityHashCode(x));//
		System.out.println(y);//20
		System.out.println(System.identityHashCode(y));//
		
	}
}
