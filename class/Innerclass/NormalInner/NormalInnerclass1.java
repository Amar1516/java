

class Outer{

	class Inner{

		void fun2(){

			System.out.println("in fun2-inner ");
		}
	}

	void fun1(){

		System.out.println("in fun1-outer");
	}
}

class Client{

	public static void main(String []args){

		Outer obj=new Outer();
		obj.fun1();

		Outer.Inner obj2=obj.new Inner();
		obj2.fun2();
	}
}
