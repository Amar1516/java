
import java.io.*;

class FileHandlingDemo{

	public static void main(String []args)throws IOException{

		File fobj =new File("Incubator.txt");
		fobj.createNewFile();

		System.out.println(fobj.getName());  // Incubator.txt
		System.out.println(fobj.getParent());// null  karan apan incubator cha parent folder nahi banvla // jya pasun apan folder/file banvto tech diste nahitar null yete outpu

		System.out.println(fobj.getPath()); //Incubator.txt same previous reason

		System.out.println(fobj.getAbsolutePath()); // /home/amar/java/class/FileHandling/Incubator.txt
		System.out.println(fobj.canRead()); // true
		
		System.out.println(fobj.canWrite()); //true

		System.out.println(fobj.isDirectory());  //false
		
		System.out.println(fobj.isFile()); //true
		
		System.out.println(fobj.list()); //null
	}
}
