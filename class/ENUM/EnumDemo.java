
enum ProgLang {

	C,CPP,Java,Python
}

class EnumDemo{

	public static void main(String []args){

		System.out.println(ProgLang.Java);
		System.out.println(ProgLang.C);
		System.out.println(ProgLang.C.ordinal());
		System.out.println(ProgLang.Python);
	}
}
