


class Parent{

	static {


		System.out.println("in parent static block");
	}
}
class Child extends Parent{

	static{
		System.out.println("In Child Static Block");
	}
}

class Client{

	public static void main(String []args){


		System.out.println("In Main Method");
		Child obj=new Child();
	}
}

