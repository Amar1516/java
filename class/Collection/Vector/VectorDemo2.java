

import java.util.*;

class VectorDemo2{

	public static void main(String []args){

		ArrayList al=new ArrayList();
		al.add(10);
		al.add(20.30);
		al.add(30.39f);
		al.add("Amar");

		for(var x:al){   // before java 10 var is not working{ var== Object Before 10 

			System.out.println(x.getClass().getName());
		}
	}
}
