
import java.util.*;

class Movies implements Comparable{

	String movieName=null;
	float totColl=0.00f;

	Movies(String movieName,float totColl){

		this.movieName=movieName;
		this.totColl=totColl;
	}
	public int compareTo(Object obj){

		return -(movieName.compareTo(((Movies)obj).movieName)); // - for reverse order  
	}

	public String toString(){

		return movieName;
	}
}
class TreeSetDemo{

	public static void main(String []args){

		TreeSet ts=new TreeSet();

		ts.add(new Movies("OMG2",200.50f));
		ts.add(new Movies("Gadar2",500.3f));
		//ts.add(new Movies("OMG2",200.50f));
		ts.add(new Movies("Jailer",1000.29f));

		System.out.println(ts);
		
	}
}
