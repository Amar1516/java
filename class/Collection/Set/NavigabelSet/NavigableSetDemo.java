
import java.util.*;

class NavigableSetDemo{

	public static void main(String []args){

		NavigableSet ns=new TreeSet();
		ns.add(20);
		ns.add(30);
		ns.add(40);
		ns.add(50);

		System.out.println(ns.descendingSet());
		System.out.println(ns.subSet(30,50));
		System.out.println(ns.subSet(30,true,50,false));
		System.out.println(ns.headSet(40));
		System.out.println(ns.tailSet(40));
		
		System.out.println(ns.lower(20));// <
		System.out.println(ns.floor(30));// <= 
		System.out.println(ns.ceiling(25)); // >=
		System.out.println(ns.higher(30));  // >
		System.out.println(ns.pollFirst());
		System.out.println(ns.pollLast());
		System.out.println(ns.iterator());
		System.out.println(ns.descendingIterator());

	
		System.out.println(ns.subSet(30,50));
		System.out.println(ns.headSet(40));
		System.out.println(ns.tailSet(40));

		
	}
}
