
//  SortedMap --> Treemap
// SortedMap Methods
import java.util.*;

class SortedMapDemo{

	public static void main(String []args){

		SortedMap sm= new TreeMap();

		sm.put("Ind","India");
		sm.put("Pak","Pakistan");
		sm.put("SL","SriLanka");
		sm.put("Aus","Australia");
		sm.put("Ban","Bangladesh");

		System.out.println(sm);

		System.out.println(sm.subMap("Aus","Pak")); // map

		System.out.println(sm.headMap("Pak"));  // map
		
		System.out.println(sm.tailMap("Pak"));  //map


		System.out.println(sm.firstKey());  // key 
		
		System.out.println(sm.lastKey());  // key
	
		System.out.println(sm.keySet());  // key list[]
		
		System.out.println(sm.values());  // values  List[]
	
		System.out.println(sm.entrySet());  // list []
	

		System.out.println(sm.remove("Aus"));  // value Australia 
		System.out.println(sm);
	}
}
