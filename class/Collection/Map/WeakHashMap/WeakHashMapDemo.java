
import java.util.*;

class Demo{

	String str;

	Demo(String str){

		this.str=str;
	}
	public String toString(){

		return str;
	}
	public void finalize(){

		System.out.println("Notify");
	}
}
class WeakHashMapDemo{

	public static void main(String []args){

		Demo obj1=new Demo("Amar");
		Demo obj2=new Demo("Bharat");
		Demo obj3=new Demo("Magar");


		obj1=null;
		obj2=null;

		System.gc();

		System.out.println("In Main");

	}
}

