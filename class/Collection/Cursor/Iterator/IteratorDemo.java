

//  Iterator is also known as Universal Iterator

import java.util.*;
class IteratorDemo{

	public static void main(String []args){

		ArrayList al=new ArrayList();

		al.add("Kanha");
		al.add("Rahul");
		al.add("Ashish");


		Iterator itr= al.iterator();  //  cursor itr will become of arrayList Type
		

		System.out.println(itr.getClass().getName()); // java.util.ArrayList$Itr

		while(itr.hasNext()){

			if("Rahul".equals(itr.next()))

				itr.remove();

				System.out.println(itr.next());  // No Such Element Found Exception
			
		}

		System.out.println(al);
	}
	

}
