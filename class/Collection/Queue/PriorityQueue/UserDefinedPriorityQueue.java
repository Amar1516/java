
import java.util.*;

class Company implements Comparable{

	String pName;
	int teamSize;
	int duration;
	Company(String pName,int teamSize,int duration){

		this.pName=pName;
		this.teamSize=teamSize;
		this.duration=duration;
	}
	public String toString(){

		return "{"+pName+":"+teamSize+","+duration+"}";
	}

	public int compareTo(Object obj){

		return pName.compareTo(((Company)obj).pName);
	}
}

class Project{

	public static void main(String []args){

		PriorityQueue pq=new PriorityQueue();
		pq.offer(new Company("WebDev",7,3));
		pq.offer(new Company("Java",9,2));
		pq.offer(new Company("Python",10,1));
		pq.offer(new Company("Dart",5,5));


		System.out.println(pq);
	}
}
