import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.TimeUnit;

class BlockingQueueDemo{

	public static void main(String []args)throws InterruptedException{

		BlockingQueue bq=new ArrayBlockingQueue(5);

		bq.put(10);
		bq.put(20);
		bq.put(30);

		System.out.println(bq);
	
		bq.offer(40,5,TimeUnit.SECONDS);

		System.out.println(bq);
		System.out.println(bq.remainingCapacity()); //  1
		System.out.println(bq.take()); // take method is called, which removes and returns the first element in the queue.

		ArrayList al=new ArrayList();


		System.out.println("ArrayList:"+al);
		
		bq.drainTo(al);//After this operation,the queue will be empty,and the ArrayList will contain the elements [20,30,40]
		System.out.println("ArrayList:"+al);
		
		System.out.println(bq);  // empty
		System.out.println(bq.remainingCapacity()); // 5
	}
}
