


//            default capacity of StringBuffer is 16  
 
class StringBufferDemo{

	public static void main(String []args){

		StringBuffer sb1 = new StringBuffer();
		StringBuffer sb2 = new StringBuffer("amar");

		System.out.println(sb1.capacity());
		System.out.println(sb2.capacity());

		System.out.println(sb1);
		System.out.println(sb2);


		sb1.append("magar");
		sb2.append("magar");

		System.out.println(sb1.capacity());
		System.out.println(sb2.capacity());
		System.out.println(sb1);
		System.out.println(sb2);

		sb2.append("nimgaonmagar"); //            new capacity= (oldcapacity+1)*2  
		System.out.println(sb2.capacity());
 		System.out.println(sb2);

	}
}
