


// 

class StringBufferDemo{

	public static void main(String [] args){

		String str1="amar";
		String str2=new String("magar");
		StringBuffer str3 =new StringBuffer("nimgaon");

	//	 String str4= str1.concat(str3);   //   error: incompatible types: StringBuffer cannot be converted to String

	//	StringBuffer str5= str3.append(str2);   //right str5 (stringBuffer) = str3(only StringBuffer).append(any type string);

		str1.concat(str2);  // str1 remains same
		str3.append(str2);  // str3 also change

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);



	}
}
