

class StringDemo{

	public static void main(String []args){

		String str1="amar";
		String str2="amar";
		String str3=new String("amar");
		String str4=new String("amar");
		String str5=new String("magar");
		String str6="magar";

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println("3&4");

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println("5&6");
		System.out.println(System.identityHashCode(str5));
		System.out.println(System.identityHashCode(str6));
}
}
