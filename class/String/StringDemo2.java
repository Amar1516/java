
class StringDemo{

	public static void main(String []args){

		String str1="AMAR";
		String str2=new String("AMAR");
		char[] str3={'A','M','A','R'};

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));	
	}
}

