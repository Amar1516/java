

class IndexOfDemo{

	public static void main(String [] args){

		String str1="amar";  //  amar --> 0,1,2,3   ,,  amar <<-- -1,-2,-3,-4
	
		System.out.println(str1.indexOf('m',0));	// 1 
		System.out.println(str1.indexOf('m',1));	// 1
	
		System.out.println(str1.indexOf('m',2));	// -1
		System.out.println(str1.indexOf('m',3));	// -1
		
		System.out.println(str1.indexOf('a'));		// 0
								
		System.out.println(str1.indexOf('a',1));	// 2
		System.out.println(str1.indexOf('a',2));	// 2

		System.out.println(str1.indexOf('a',3));	// -1
		System.out.println(str1.indexOf('a',4));	// -1
	}
}
