
// lastIndexOf('char',UptoRange);

class LastIndexOfDemo{

	public static void main(String [] args){

		String str="amar";
		String str1="shashi";
		System.out.println(str.lastIndexOf('a',0));	 //0
		System.out.println(str.lastIndexOf('a',1)); 	//0
		System.out.println(str.lastIndexOf('a',2)); 	//2
		System.out.println(str.lastIndexOf('a',3)); 	//2

		System.out.println(str1.lastIndexOf('h',5));	//4
	}
}
