

class Demo{

	void amar(int x){

		System.out.println(x);
	}

	void amar(float y){

		System.out.println(y);
	}

	void amar(Demo obj){

		System.out.println("in demo parameter");
		System.out.println(obj);

	}

	public static void main(String [] args){

		Demo magar=new Demo();

		magar.amar(10);
		magar.amar(10.f);

		Demo magar2=new Demo();
		magar2.amar(magar);

	}
}

/*  o/p:
10
10.5
in demo parameter
Demo@7ad041f3

*/
