
/*
 1] Search an Element in an array.
Given an integer array and another integer element. The task is to find if the given
element is present in the array or not. if yes print index of element.
  */

import java.util.*;
class SearchElement{

	public static void main(String []args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter The Size of Array:");
		int size =sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter The Elements of Array:");
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter Element to Search:");
		int ele=sc.nextInt();

		for(int i=0;i<arr.length;i++){
			if(arr[i]==ele){

				System.out.println("Element Found At Index:"+i);
				break;
			}else{
				if(i==arr.length-1){
					System.out.println("Element Not Found");
					break;
				}
			}
		}
	}
}
