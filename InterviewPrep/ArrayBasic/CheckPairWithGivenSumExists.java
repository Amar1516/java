/*
 * 20] Check if pair with given Sum exists in Array (Two Sum)
Given an array A[] of n numbers and another number x, the task is to check
whether or not there exist two elements in A[] whose sum is exactly x.
Examples:
Input: arr[] = {0, -1, 2, -3, 1}, x= -2
Output: Yes
Explanation: If we calculate the sum of the output,1 + (-3) = -2
Input: arr[] = {1, -2, 1, 0, 5}, x = 0
Output: No
*/

import java.util.*;
class CheckPairWithGivenSumExists{
	public static void main(String []args){


		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The SIze OF Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter The Elements Of Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter The Number (sum):");
		int sum=sc.nextInt();
		int tempSum=0;
		boolean bool=false;
		int count=0;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(((arr[i] + arr[j])==sum) && (i!=j)){
					count++;
					//bool=true;
				}
			}
		}
		if(count>=1){
			System.out.println("Yes");
		}else{
			System.out.println("No");	
		}


	}
}
