
//Product of array elements
import java.util.*;

class ProductOfArray{

	public static void main(String []args){

		Scanner sc =new Scanner(System.in);

		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];

		int prod=1;
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
			prod=prod*arr[i];
		}

		System.out.println("The Product Of Array Element is:"+prod);

	}
}

