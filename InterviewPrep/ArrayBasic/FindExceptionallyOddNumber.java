
/*22] Exceptionally odd
Given an array of N positive integers where all numbers occur even number of
times except one number which occurs odd number of times. Find the exceptional
number.
Example 1:
Input:
N = 7
Arr[] = {1, 2, 3, 2, 3, 1, 3}
Output: 3
Explanation: 3 occurs three times*/

import java.util.*;

class ExceptionallyOdd{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size OF Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter ELements OF Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int oddNo=exceptionallyOddNo(arr);
		if(oddNo!=0){
			System.out.println("Occurs :"+ oddNo);
		}
	}
	
		static int exceptionallyOddNo(int arr[]){
		
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if( arr[i]==arr[j]){
					count++;
				}
			}
			if(count%2==1){
				return arr[i];
			}
		}

		return 0;
	
		}
}

