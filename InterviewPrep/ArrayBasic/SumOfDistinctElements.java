/*15] Sum of distinct elements
You are given an array Arr of size N. Find the sum of distinct elements in an array.
Example 1:
Input:
N = 5
Arr[] = {1, 2, 3, 4, 5}
Output: 15
Explanation: Distinct elements are 1, 2, 3
4, 5. So the sum is 15.
Example 2:
Input:
N = 5
Arr[] = {5, 5, 5, 5, 5}
Output: 5
Explanation: Only Distinct element is 5.
So the sum is 5
*/

import java.util.*;
class SumOfDistinctElements{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the Elements of array:");

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		int sum=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count==1){
				sum=sum+arr[i];
			}
			if(sum==0 && (i==arr.length-1)){
				sum=arr[i];
			}
		}
		System.out.println("Sum Of Distinct element is:"+sum);	
	}

}
