
/*26] Positive and negative elements
Given an array arr[ ] containing equal number of positive and negative elements,
arrange the array such that every positive element is followed by a negative
element.
Note- The relative order of positive and negative numbers should be maintained.
Example 1:
Input:
N = 6
arr[] = {-1, 2, -3, 4, -5, 6}
Output:
2 -1 4 -3 6 -5
Explanation: Positive numbers in order are 2, 4 and 6. Negative numbers in
order are -1, -3 and -5. So the arrangement we get is 2, -1, 4, -3, 6 and -5.
Example 2:
Input:
N = 4
arr[] = {-3, 2, -4, 1}
Output:
2 -3 1 -4
*/

import java.util.*;

class PositiveAndNegative{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter The Elements Of Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		rearrangeArray(arr);
	
	}
	static void rearrangeArray(int arr[]){
			
			int positive[]=new int[arr.length/2];
			int negative[]=new int[arr.length/2];

			int pIndex=0;
			int nIndex=0;

		for(int i=0;i<arr.length;i++){
			if(arr[i]>0){
				positive[pIndex++] =arr[i];
			}else{
				negative[nIndex++]=arr[i];
				}
			}
		

		 pIndex=0;
		 nIndex=0;

		 for(int i=0;i<arr.length;i++){
			 if(i%2==0){
				 arr[i]=positive[pIndex++];
			 }else{
				 arr[i]=negative[nIndex++];
			 }
		 }


		System.out.println("Output is : ");

		 for(int data:arr){
			 System.out.print(data+" ");
		 }
	}
}






















	
