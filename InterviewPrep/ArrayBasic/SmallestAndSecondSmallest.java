
/*23] Find the smallest and second smallest element in an array
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.*/

import java.util.*;

class Smallest{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size OF Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter Elements OF Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int min=arr[0];
		int min1=arr[1];
		for(int i=1;i<arr.length;i++){
			if(min==min1){
				min1=arr[i+1];
			}
			if(arr[i]<min){
				min1=min;
				min=arr[i];
			}else if(arr[i]<min1 && arr[i]!=min){
				min1=arr[i];
			}
		}
		System.out.println("Smallest No:"+min);
		System.out.println("Second Smallest No:"+min1);
	}
}
