/*Given an array Arr of size N, the array contains numbers in range from 0 to K-1
where K is a positive integer and K <= N. Find the maximum repeating number in
this array. If there are two or more maximum repeating numbers return the element
having least value.
Example 1:
Input:
N = 4, K = 3
Arr[] = {2, 2, 1, 2}
Output: 2
Explanation: 2 is the most frequent element.
*/
/*
import java.util.*;
 class MaxRepeatingNumber{
	 public static void main(String []args){
		 Scanner sc =new Scanner(System.in);
		 System.out.println("Enter The Size Of Array:");
		 int size=sc.nextInt();
		 int arr[]=new int[size];
		 System.out.println("Elements Range from 0 to  :"+(size-1));
		 int range=sc.nextInt();

		 System.out.println("Enter The Elements Between 0 to :"+range);
		 for(int i=0;i<arr.length;i++){
			 arr[i]=sc.nextInt();
		 }
		 int arr2[]=new int[arr.length];
		 for(int i=0;i<arr.length;i++){
			 int count=0;
			 for(int j=0;j<arr.length;j++){

				 if(arr[i]==arr[j]){

					 count++;
				 }
			 }
			 arr2[i]=count;
		 }
	 }
 }
*/

class MaxRepeatingNumber {
    public static void main(String[] args) {
        int[] arr1 = {2, 2, 1, 2};
        int n1 = 4, k1 = 3;
        int result1 = findMaxRepeatingNumber(arr1, n1, k1);
        System.out.println("Output 1: " + result1);

        int[] arr2 = {2, 2, 1, 0, 0, 1};
        int n2 = 6, k2 = 3;
        int result2 = findMaxRepeatingNumber(arr2, n2, k2);
        System.out.println("Output 2: " + result2);
    }

     static int findMaxRepeatingNumber(int[] arr, int n, int k) {
        int[] frequency = new int[k];

        for (int i = 0; i < n; i++) {
            frequency[arr[i]]++;
        }

        int maxFrequency = -1;
        int maxRepeatingNumber = -1;

        for (int i = 0; i < k; i++) {
            if (frequency[i] > maxFrequency || (frequency[i] == maxFrequency && i < maxRepeatingNumber)) {
                maxFrequency = frequency[i];
                maxRepeatingNumber = i;
            }
        }

        return maxRepeatingNumber;
    }
}
