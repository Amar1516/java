
/*Find minimum and maximum element in an array

Given an array A of size N of integers. Your task is to find the minimum and
maximum elements in the array.
*/
import java.util.*;

class MinAndMaxElement{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter The Elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		int minEle=arr[0];
		int maxEle=arr[0];

		for(int i=1;i<arr.length;i++){
			if(arr[i]<minEle){

				minEle=arr[i];
			}
			if(arr[i]>maxEle){

				maxEle=arr[i];
			}
		}

		System.out.println("Minimum Element is :" + minEle +" & Maximum Element is :" + maxEle);
	}
}
