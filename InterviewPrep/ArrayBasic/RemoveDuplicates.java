

/*28] Remove Duplicates from unsorted array
Given an array of integers which may or may not contain duplicate elements. Your
task is to remove duplicate elements, if present.
Example 1:
Input:
N = 6
A[] = {1, 2, 3, 1, 4, 2}
Output:
1 2 3 4
Example 2:
Input:
N = 4
A[] = {1, 2, 3, 4}
Output:
1 2 3 4
*/

import java.util.*;

class RemoveDuplicates{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter The Elements of  Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		removeDuplicate(arr);
		
	}

	static void  removeDuplicate(int arr[]){
	
		int arr2[]=new int[arr.length];
		int temp=0;
		int count =0;

		for(int i=0;i<arr.length;i++){

			boolean flag=false;
			for(int j=0;j<temp;j++){

				if(arr[i]==arr[j]){
					flag=true;
					break;
				}
			}
			if(!flag){
				arr2[temp++]=arr[i];
				count++;
		}
	}

	for(int data:arr2){

		System.out.print(data+" ");
	}
}

}



































