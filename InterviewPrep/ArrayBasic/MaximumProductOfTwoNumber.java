/*
 * 25] Maximum product of two numbers
Given an array Arr of size N with all elements greater than or equal to zero. Return
the maximum product of two numbers possible.
Example 1:
Input:
N = 6
Arr[] = {1, 4, 3, 6, 7, 0}
Output: 42
Example 2:
Input:
N = 5
Arr = {1, 100, 42, 4, 23}
Output: 4200
*/
import java.util.*;

class MaximumProduct{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter The Elements Of Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		if(arr.length>1){

		int max=arr[0];
		int max1=arr[1];
		for(int i=1;i<arr.length;i++){
			if(arr[i]>max){
				max1=max;
				max=arr[i];
			}else if(arr[i]>max1 ){          //&& arr[i]!=max){
				max1=arr[i];
			}

		}

		System.out.println("Maximum Product of Two Numbers: "+max*max1);

		}
		else{
		System.out.println("Please Enter valid Size of Array: ");

	}
}
}
