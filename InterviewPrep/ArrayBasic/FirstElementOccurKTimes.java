/*
 * 21] First element to occur k times
Given an array of N integers. Find the first element that occurs at least K number
of times.
Example 1:
Input :
N = 7, K = 2
A[] = {1, 7, 4, 3, 4, 8, 7}
Output :
4
Explanation:
Both 7 and 4 occur 2 times.
But 4 is first that occurs 2 times
As at index = 4, 4 has occurred
at least 2 times whereas at index = 6,
7 has occurred at least 2 times.
*/

import java.util.*;
class FirstElementOccur{

	public static void main(String []args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter The Elements of Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter No. how many times Number Occur:");
		int k=sc.nextInt();

		int result= findElementWithKOccurence(arr,k);
		
		if(result!=-1){
			System.out.println("First Element That Occurs at least "+k+" times is "+result);
		}else{
			
		System.out.println("No Elements Occurs at least "+k+"Times");
		}
	}
			

	static int findElementWithKOccurence(int []arr,int k){

		for(int i=0;i<arr.length;i++){

			int count =0;
			for(int j=0;j<arr.length;j++){
				if(i != j && arr[i]==arr[j]){
					count++;
				}
			}
			
			if(count == k-1){
				return arr[i] ;	
			}
		}
		return -1;
	}
}

