
/*13] Find unique element

  Given an array of size n which contains all elements occurring in multiples of K,
except one element which doesn't occur in multiple of K. Find that unique element.
Example 1:
Input :
n = 7, k = 3
arr[] = {6, 2, 5, 2, 2, 6, 6}
Output :
5
Explanation:
Every element appears 3 times except 5.

*/

import java.util.*;
class UniqueElement{

	public  static void main(String []args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int temp=0;
		
		System.out.println("Enter The Number To find :");
		int num=sc.nextInt();
		
		System.out.println("Enter The Elements Of Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
	
		int sum=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
		}
		/*
		if(count != num){
		System.out.println("  "+arr[i]);
	}*/
	if(count==1){
		temp=arr[i];
	}
	}

	if(temp==0){
		System.out.println("-1");
	}else{
	System.out.println("Uniqe Number is:"+temp);
	}

	}
}

/*
import java.util.Scanner;

class UniqueElement {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter The Size Of Array:");
        int size = sc.nextInt();
        System.out.println("Enter The Number, Multiples of Elements Occurring");
        int num = sc.nextInt();
        int arr[] = new int[size];
        System.out.println("Enter The Elements Of Array:");

        // Input the elements into the array
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        int uniqueElement = findUniqueElement(arr, num);
        System.out.println("Unique Element: " + uniqueElement);
    }

    public static int findUniqueElement(int[] arr, int num) {
        for (int i = 0; i < arr.length; i++) {
            int count = 0;

            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    count++;
                }
            }

            if (count != num) {
                return arr[i];
            }
        }

        return -1; // If no unique element is found
    }
}*/
