

//Write a java program to check if a number is even or odd.
import java.io.*;

class EvenOdd{

	public static void main(String []args)throws IOException{

		int num;
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter The Number:");

		num= Integer.parseInt(br.readLine());

		if(num>0 && num%2==0){

			System.out.println("Number is Even");
		}else if(num>0 && num%2!=0){
			System.out.println("Number is Odd");
		}else if(num<0){
			System.out.println("Number is Negative");
		}else{
			System.out.println("Entered No is Zero");
		}
	}
}
